<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp2');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '5~1`^z:|n,2r=O!>~cS)9J3W>G*%4isy?Jcz_ctk<J6!3kX^Uwn~?hr7s^ XXRFW');
define('SECURE_AUTH_KEY',  '-gRD^q?= l)@ML7YO~Z=76@piS/I6&(WQ,.,nwC@Z~dkQT~ I5(G Wb>2m0s3Rx1');
define('LOGGED_IN_KEY',    '-;5?@GcSc,-Q%;A6[M?|9#;d.16]Y]bZ6!c#@Eq^I/R5s_$3y2[K/A4Iob)nl*pM');
define('NONCE_KEY',        'Y`Ckdu2/faMIvT6l?(N|)6:pHEAAR7u?CCp#G<x^S_i0}s+h_}|N<o!N4 .7XtyV');
define('AUTH_SALT',        '>l<3}H+kH|lDFB9:tpET:iE qFvO/{b-@~Xij;v[*VqZfS,X43Di4zkkZ@^4hc2x');
define('SECURE_AUTH_SALT', '&ijG6QRkqat?FPoNK)=PNx_0#>iFJ(ID1EQPp]bg5,0KUiOcUZI^t$,?#)DD8U2^');
define('LOGGED_IN_SALT',   '?q8}9P#&lNTrZ6Z+~]8Wn`GZS?soe1=F][4<Y!%n~>}KkG&.IgW{:&D7wChr* S(');
define('NONCE_SALT',       '<8|ja/E/F:Y@uHYtMC0=Ln?;%CC[OQHD|zhn@#_W(Ri{[YG2!d_RLGHvSnkU6@`2');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
