<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ep
 */

?>

	  </div><!-- #content_resize -->
	</div><!-- #content -->
  <div class="fbg">
    <div class="fbg_resize">
      <div class="col c1">
        <h2><span>Image Gallery</span></h2>
        <a href="#"><img src="/wp-content/themes/ep/assets/images/pix1.jpg" width="56" height="56" alt="" /></a> <a href="#"><img src="/wp-content/themes/ep/assets/images/pix2.jpg" width="56" height="56" alt="" /></a> <a href="#"><img src="/wp-content/themes/ep/assets/images/pix3.jpg" width="56" height="56" alt="" /></a> <a href="#"><img src="/wp-content/themes/ep/assets/images/pix4.jpg" width="56" height="56" alt="" /></a> <a href="#"><img src="/wp-content/themes/ep/assets/images/pix5.jpg" width="56" height="56" alt="" /></a> <a href="#"><img src="/wp-content/themes/ep/assets/images/pix6.jpg" width="56" height="56" alt="" /></a> </div>
      <div class="col c2">
        <h2><span>Lorem Ipsum</span></h2>
        <p>Lorem ipsum dolor<br />
          Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec libero. Suspendisse bibendum. Cras id urna. <a href="#">Morbi tincidunt, orci ac convallis aliquam</a>, lectus turpis varius lorem, eu posuere nunc justo tempus leo. Donec mattis, purus nec placerat bibendum, dui pede condimentum odio, ac blandit ante orci ut diam.</p>
      </div>
      <div class="col c3">
        <h2><span>About</span></h2>
        <img src="/wp-content/themes/ep/assets/images/white.jpg" width="56" height="56" alt="" />
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec libero. Suspendisse bibendum. Cras id urna. Morbi tincidunt, orci ac convallis aliquam, lectus turpis varius lorem, eu posuere nunc justo tempus leo. llorem, eu posuere nunc justo tempus leo. Donec mattis, purus nec placerat bibendum. <a href="#">Learn more...</a></p>
      </div>
      <div class="clr"></div>
    </div>
  </div>
  <div class="footer">
    <div class="footer_resize">
      <p class="lf"><a href="<?php echo esc_url( __( 'https://wordpress.org/', 'ep' ) ); ?>"><?php
				/* translators: %s: CMS name, i.e. WordPress. */
				printf( esc_html__( 'Proudly powered by %s', 'ep' ), 'WordPress' );
			?></a>. All Rights Reserved</p>
      <p class="rf">Design by<?php
				/* translators: 1: Theme name, 2: Theme author. */
				printf( esc_html__( 'Theme: %1$s by %2$s.', 'ep' ), 'ep', '<a href="http://underscores.me/">Underscores.me</a>' );
			?>
      </p>
      <div class="clr"></div>
    </div>
  </div>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
