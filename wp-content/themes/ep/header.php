<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ep
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

  <script>
    document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] +
    ':35729/livereload.js?snipver=1"></' + 'script>')
  </script> 
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div id="page" class="site">
	<header id="masthead" class="site-header">
    <div class="header_resize">
     <div class="logo">
        <h1><a href="#"><span>Duet</span> Plasma <small>Put your best slogan here</small></a></h1>
      </div>
      <nav id="site-navigation" class="main-navigation">
        <?php
          wp_nav_menu( array(
            'theme_location' => 'menu-1',
            'menu_id'        => 'primary-menu',
          ) );
        ?>
      </nav><!-- #site-navigation -->
      <div class="clr"></div>
		</div><!-- .header_resize -->
	</header><!-- #masthead -->

  <div class="hbg">&nbsp;</div>
  <div id="content" class="content">
    <div class="content_resize">
