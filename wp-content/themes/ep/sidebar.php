<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ep
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>


<div class="sidebar">
  <div class="gadget">
	  <?php dynamic_sidebar( 'sidebar-1' ); ?>
    <h2 class="star"><span>Sidebar</span> Menu</h2>
    <div class="clr"></div>
    <ul class="sb_menu">
      <li><a href="#">Home</a></li>
      <li><a href="#">TemplateInfo</a></li>
      <li><a href="#">Style Demo</a></li>
      <li><a href="#">Blog</a></li>
      <li><a href="#">Archives</a></li>
      <li><a href="#">Web Templates</a></li>
    </ul>
  </div>
  <div class="gadget">
    <h2 class="star"><span>Sponsors</span></h2>
    <div class="clr"></div>
    <ul class="ex_menu">
      <li><a href="#">Lorem ipsum dolor</a><br />
        Donec libero. Suspendisse bibendum</li>
      <li><a href="#">Donec mattis</a><br />
        Phasellus suscipit, leo a pharetra</li>
      <li><a href="#">Dui pede condimentum</a><br />
        Tellus eleifend magna eget</li>
      <li><a href="#">Condimentum lorem</a><br />
        Curabitur vel urna in tristique</li>
      <li><a href="#">Fringilla velit magna</a><br />
        Cras id urna orbi tincidunt orci ac</li>
      <li><a href="#">Suspendisse bibendum</a><br />
        purus nec placerat bibendum</li>
    </ul>
  </div>
</div>
<div class="clr"></div>
